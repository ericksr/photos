<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
        @yield('meta')
        <title>@yield('title')</title>

        <!-- CSS  -->
        <link href="{{asset('css/icon')}}" rel="stylesheet">
        <link href="{{asset('css/materialize.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="{{asset('css/style.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
        @yield('styles')
    </head>
    <body>
        @section('nav')
        <nav class="black" role="navigation">
            <div class="nav-wrapper container">
                <a id="logo-container" href="#" class="brand-logo">Mi Galería</a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="#">@lang('messages.login_label')</a></li>
                </ul>

                <ul id="nav-mobile" class="side-nav" style="transform: translateX(-100%);">
                    <li><a href="#">@lang('messages.login_label')</a></li>
                </ul>
                <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
            </div>
        </nav>
        @show
        <div class="section no-pad-bot" id="index-banner">
            <div class="container">
                @yield('content')

            </div>
        </div>
        @section('footer')
        <footer class="page-footer yellow">
            <div class="container">
                <div class="row">
                    <div class="col l6 s12">
                        <p class="black-text">&copy; Mi Galería 2017</p>
                    </div>
                </div>
            </div>
        </footer>
        @show

        <!--  Scripts-->
        <script src="{{asset('js/jquery-3.2.1.js')}}"></script>
        <script src="{{asset('js/materialize.js')}}"></script>
        <script src="{{asset('js/init.js')}}"></script>
        @yield('scripts')



        <div class="hiddendiv common"></div><div class="drag-target" data-sidenav="nav-mobile" style="left: 0px; touch-action: pan-y; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></div></body></html>
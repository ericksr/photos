@extends('template.master')
@section('title','Mi Galería')
@section('content')
<h1 class="header center-align">@lang('messages.index_header_label')</h1>
<div class="carousel carousel-slider">
    <a class="carousel-item" href="#one!"><img src="http://lorempixel.com/800/400/food/1"></a>
    <a class="carousel-item" href="#two!"><img src="http://lorempixel.com/800/400/food/2"></a>
    <a class="carousel-item" href="#three!"><img src="http://lorempixel.com/800/400/food/3"></a>
    <a class="carousel-item" href="#four!"><img src="http://lorempixel.com/800/400/food/4"></a>
  </div>
@endsection
@section('scripts')
<script src="{{asset('js/home.js')}}"></script>
@endsection